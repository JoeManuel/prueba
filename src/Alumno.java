
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
*
 * @author FACCI
 */
public class Alumno {
 // Creaciòn de propiedades.
 private String Nombre; 
 private String Apeliido;
 private String Cedula;
 private Date Fechadenacimiento;
 //Creaciòn de metòdos

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApeliido() {
        return Apeliido;
    }

    public void setApeliido(String Apeliido) {
        this.Apeliido = Apeliido;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public Date getFechadenacimiento() {
        return Fechadenacimiento;
    }

    public void setFechadenacimiento(Date Fechadenacimiento) {
        this.Fechadenacimiento = Fechadenacimiento;
    }
 
 

}
