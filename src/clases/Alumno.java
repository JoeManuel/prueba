/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.Date;

/**
 *
 * @author FACCI
 */
class Alumno {
    //creaciòn de propiedades
   private String nombre;
   private String apellido;
   private String cedula;
   private Date fechaDeNacimiento;
   
   //creaciòn del constructor

    public Alumno() {
        this.nombre = "Joe";
        this.apellido = "Pinargote";
    }
   
   
   //creaciòn de mètodos

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setFechaDeNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }
   
}
